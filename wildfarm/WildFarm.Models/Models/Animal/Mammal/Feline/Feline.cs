﻿using System;
using WildFarm.Models.Contracts.Animal.Mammal.Feline;

namespace WildFarm.Models.Models.Animal.Mammal.Feline
{
    public abstract class Feline : Mammal, IFeline
    {
        private string _breed;
        private string breedName = nameof(Breed);
        public Feline(string name, double weight, string livingRegion, string breed)
            : base(name, weight, livingRegion)
        {
            this.Breed = breed;
        }

        public string Breed 
        {
            get => this._breed;
            private set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException($"{breedName} cannot be null or white space!");
                }
                this._breed = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + $"{this.Breed}, {base.Weight}, {base.LivingRegion}, {base.FoodEaten}]";
        }
    }
}
