﻿using WildFarm.Models.Contracts.Food;

namespace WildFarm.Models.Contracts.Animal
{
    public interface IAnimal
    {
        string Name { get; }
        double Weight { get; }
        double FoodEaten { get; }
        abstract string ProducingSound();
        abstract string Eat(IFood food);
    }
}