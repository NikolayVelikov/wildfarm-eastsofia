﻿namespace WildFarm.Models.Contracts.Animal.Bird
{
    public interface IBird : IAnimal
    {
        public double WingSize { get; }
    }
}