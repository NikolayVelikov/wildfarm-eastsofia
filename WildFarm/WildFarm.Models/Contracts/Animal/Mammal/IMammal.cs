﻿namespace WildFarm.Models.Contracts.Animal
{
    public interface IMammal : IAnimal
    {
        string LivingRegion { get; }
    }
}
