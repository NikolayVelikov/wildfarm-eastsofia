﻿namespace WildFarm.Models.Contracts.Animal.Mammal.Feline
{
    public interface IFeline : IMammal
    {
        string Breed { get; }
    }
}
