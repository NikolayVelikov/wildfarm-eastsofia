﻿namespace WildFarm.Models.Contracts.Food
{
    public interface IFood
    {
         int Quantity { get;}
    }
}
