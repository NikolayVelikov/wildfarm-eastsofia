﻿namespace WildFarm.Models.DefaultValues
{
    public static class Manupulation
    {
        public const double hen = 0.35;
        public const double owl = 0.25;
        public const double mouse = 0.10;
        public const double cat = 0.30;
        public const double dog = 0.40;
        public const double tiger = 1.00;
    }
}