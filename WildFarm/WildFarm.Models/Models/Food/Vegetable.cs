﻿namespace WildFarm.Models.Models.Food
{
    public class Vegetable : Food
    {
        public Vegetable(int foodQuantity) 
            : base(foodQuantity)
        {
        }
    }
}
