﻿using System;
using WildFarm.Models.Contracts.Food;
namespace WildFarm.Models.Models.Food
{
    public abstract class Food : IFood
    {
        protected int _food;
        protected string quantityName = nameof(Quantity);
        public Food(int foodQuantity)
        {
            this.Quantity = foodQuantity;
        }
        public int Quantity 
        {
            get => this._food;
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException($"{quantityName} cannot be negative number!");
                }
                this._food = value;
            }
        }
    }
}
