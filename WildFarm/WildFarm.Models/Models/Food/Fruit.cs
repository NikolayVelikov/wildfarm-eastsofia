﻿namespace WildFarm.Models.Models.Food
{
    public class Fruit : Food
    {
        public Fruit(int foodQuantity) 
            : base(foodQuantity)
        {
        }
    }
}
