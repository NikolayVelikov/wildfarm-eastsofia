﻿using System;

using WildFarm.Models.Contracts.Animal.Bird;

namespace WildFarm.Models.Models.Animal.Bird
{
    public abstract class Bird : Animal, IBird
    {
        private double _wingSize;
        protected Bird(string name, double weight, double wingSize)
            : base(name, weight)
        {
            this.WingSize = wingSize;
        }

        public double WingSize
        {
            get => this._wingSize;
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Wing size can not be less than zero !!!");
                }

                this._wingSize = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + $"{this.WingSize}, {base.Weight}, {base.FoodEaten}]";
        }
    }
}