﻿using System;
using WildFarm.Models.Messages;
using WildFarm.Models.Models.Food;
using WildFarm.Models.DefaultValues;
using WildFarm.Models.Contracts.Food;

namespace WildFarm.Models.Models.Animal.Bird
{
    public class Owl : Bird
    {
        private const double weight = Manupulation.owl;
        public Owl(string name, double weight, double wingSize)
            : base(name, weight, wingSize)
        {
        }

        public override string Eat(IFood food)
        {
            if (food.GetType().Name != nameof(Meat))
            {
                throw new InvalidOperationException(string.Format(ErrorOutputMessages.notEat, nameof(Owl), food.GetType().Name));
            }

            double eaten = food.Quantity * weight;
            base.Weight += eaten;

            base.FoodEaten += food.Quantity;

            return ErrorOutputMessages.eat;
        }

        public override string ProducingSound()
        {
            return "Hoot Hoot";
        }
    }
}