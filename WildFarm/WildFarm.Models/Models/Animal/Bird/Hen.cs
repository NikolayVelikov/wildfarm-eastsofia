﻿using WildFarm.Models.Contracts.Food;
using WildFarm.Models.DefaultValues;
using WildFarm.Models.Messages;

namespace WildFarm.Models.Models.Animal.Bird
{
    public class Hen : Bird
    {
        private const double weight = Manupulation.hen;
        public Hen(string name, double weight, double wingSize)
            : base(name, weight, wingSize)
        {
        }

        public override string Eat(IFood food)
        {
            double eaten = food.Quantity * weight;
            base.Weight += eaten;

            base.FoodEaten += food.Quantity;

            return ErrorOutputMessages.eat;
        }

        public override string ProducingSound()
        {
            return "Cluck";
        }
    }
}