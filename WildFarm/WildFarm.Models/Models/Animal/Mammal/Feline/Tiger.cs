﻿using System;
using WildFarm.Models.Contracts.Food;
using WildFarm.Models.DefaultValues;
using WildFarm.Models.Messages;
using WildFarm.Models.Models.Food;

namespace WildFarm.Models.Models.Animal.Mammal.Feline
{
    public class Tiger : Feline
    {
        private const double weight = Manupulation.tiger;
        public Tiger(string name, double weight, string livingRegion, string breed)
            : base(name, weight, livingRegion, breed)
        {
        }

        public override string Eat(IFood food)
        {
            string foodName = food.GetType().Name;
            if (foodName != nameof(Meat))
            {
                throw new InvalidOperationException(string.Format(ErrorOutputMessages.notEat, nameof(Tiger), foodName));
            }

            double eaten = food.Quantity * weight;
            base.Weight += eaten;

            base.FoodEaten += food.Quantity;

            return ErrorOutputMessages.eat;
        }

        public override string ProducingSound()
        {
            return "ROAR!!!";
        }
    }
}