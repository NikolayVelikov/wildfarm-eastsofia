﻿using System;
using WildFarm.Models.Contracts.Animal;

namespace WildFarm.Models.Models.Animal.Mammal
{
    public abstract class Mammal : Animal, IMammal
    {
        private string _livingRegion;
        private string livingRegionName = "Living Region";
        protected Mammal(string name, double weight,string livingRegion)
            : base(name, weight)
        {
            this.LivingRegion = livingRegion;
        }

        public string LivingRegion
        {
            get => this._livingRegion;
            private set 
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException($"{livingRegionName} cannot be null or whitespace!");
                }
                this._livingRegion = value;
            }
        }
    }
}
