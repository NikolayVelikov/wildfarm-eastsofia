﻿using System;
using WildFarm.Models.Contracts.Food;
using WildFarm.Models.DefaultValues;
using WildFarm.Models.Messages;
using WildFarm.Models.Models.Food;

namespace WildFarm.Models.Models.Animal.Mammal
{
    public class Mouse : Mammal
    {
        private string mouseSound = "Squeak";
        private const double weight = Manupulation.mouse;

        public Mouse(string name, double weight,  string livingRegion)
            : base(name, weight,  livingRegion)
        {
        }

        public override string Eat(IFood food)
        {
            if (food.GetType().Name != nameof(Vegetable) && food.GetType().Name != nameof(Fruit))
            {
                throw new InvalidOperationException(string.Format(ErrorOutputMessages.notEat, nameof(Mouse), food.GetType().Name));
            }

            double eaten = food.Quantity * weight;
            base.Weight += eaten;

            base.FoodEaten += food.Quantity;

            return ErrorOutputMessages.eat;
        }

        public override string ProducingSound()
        {
            return mouseSound;
        }

        public override string ToString()
        {
            return base.ToString() + $"{base.Weight}, {base.LivingRegion}, {base.FoodEaten}]";
        }
    }
}
