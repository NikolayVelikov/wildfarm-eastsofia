﻿using System;
using WildFarm.Models.Contracts.Food;
using WildFarm.Models.DefaultValues;
using WildFarm.Models.Messages;
using WildFarm.Models.Models.Food;

namespace WildFarm.Models.Models.Animal.Mammal
{
    public class Dog : Mammal
    {
        private const double weight = Manupulation.dog;
        private string dogSound = "Woof!";
        public Dog(string name, double weight, string livingRegion) 
            : base(name, weight, livingRegion)
        {
        }

        public override string Eat(IFood food)
        {
            string foodName = food.GetType().Name;
            if (foodName != nameof(Meat))
            {
                throw new InvalidOperationException(string.Format(ErrorOutputMessages.notEat, nameof(Dog), foodName));
            }

            double eaten = food.Quantity * weight;
            base.Weight += eaten;

            base.FoodEaten += food.Quantity;

            return ErrorOutputMessages.eat;
        }

        public override string ProducingSound()
        {
            return dogSound;
        }

        public override string ToString()
        {
            return base.ToString() + $"{base.Weight}, {base.LivingRegion}, {base.FoodEaten}]";
        }
    }
}
