﻿using System;
using WildFarm.Models.Contracts.Animal;
using WildFarm.Models.Contracts.Food;

namespace WildFarm.Models.Models.Animal
{
    public abstract class Animal : IAnimal
    {
        private string _name;
        private double _weight;

        protected Animal(string name, double weight)
        {
            this.Name = name;
            this.Weight = weight;
        }
        public string Name
        {
            get => this._name;
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException($"Name can not be null or empty !!!");
                }

                this._name = value; ;
            }
        }

        public double Weight
        {
            get => this._weight;
            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Weight can not be negative or zero !!!");
                }

                this._weight = value; ;
            }
        }

        public double FoodEaten { get; protected set; }

        public abstract string Eat(IFood food);

        public abstract string ProducingSound();

        public override string ToString()
        {
            return $"{this.GetType().Name} [{this.Name}, ";
        }
    }
}