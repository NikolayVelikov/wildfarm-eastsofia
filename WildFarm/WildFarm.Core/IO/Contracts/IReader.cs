﻿namespace WildFarm.Core.IO.Contracts
{
    public interface IReader
    {
        string ReadLine();
    }
}