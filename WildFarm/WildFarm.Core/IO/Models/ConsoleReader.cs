﻿using WildFarm.Core.IO.Contracts;

namespace WildFarm.Core.IO.Models
{
    public class ConsoleReader : IReader
    {
        public string ReadLine()
        {
            return System.Console.ReadLine();
        }
    }
}