﻿using WildFarm.Core.IO.Contracts;

namespace WildFarm.Core.IO.Models
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string text)
        {
            System.Console.Write(text);
        }

        public void WriteLine(string text)
        {
            System.Console.WriteLine(text);
        }
    }
}