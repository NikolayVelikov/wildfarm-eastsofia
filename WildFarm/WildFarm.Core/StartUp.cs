﻿using WildFarm.Core.Contracts;
using WildFarm.Core.Core;
using WildFarm.Core.IO.Contracts;
using WildFarm.Core.IO.Models;

namespace WildFarm.Core
{
    class StartUp
    {
        static void Main(string[] args)
        {
            IReader reader = new ConsoleReader();
            IWriter writer = new ConsoleWriter();
            IRepository repo = new Repos();
            IService service = new Service(repo);
            ICommandInterpertator interpreter = new CommandInterpretator(service);

            IEngine engine = new Engine(reader,writer,interpreter);

            engine.Run();
        }
    }
}
