﻿using System;
using System.Text;
using WildFarm.Core.Contracts;
using WildFarm.Models.Contracts.Animal;
using WildFarm.Models.Contracts.Animal.Bird;
using WildFarm.Models.Contracts.Animal.Mammal.Feline;
using WildFarm.Models.Contracts.Food;
using WildFarm.Models.Models.Animal.Bird;
using WildFarm.Models.Models.Animal.Mammal;
using WildFarm.Models.Models.Animal.Mammal.Feline;
using WildFarm.Models.Models.Food;

namespace WildFarm.Core.Core
{
    public class Service : IService
    {
        private IRepository _repo;
        public Service(IRepository reposutory)
        {
            this._repo = reposutory;
        }

        public string CreateCat(string name, double weight, string livingRegion, string breed)
        {
            IFeline cat = new Cat(name, weight, livingRegion, breed);

            this._repo.AddAnimal(cat);

            return cat.ProducingSound();
        }
        public string CreateDog(string name, double weight, string livingRegion)
        {
            IMammal dog = new Dog(name, weight, livingRegion);

            this._repo.AddAnimal(dog);

            return dog.ProducingSound();
        }
        public string CreateTiger(string name, double weight, string livingRegion, string breed)
        {
            IFeline tiger = new Tiger(name, weight, livingRegion, breed);

            this._repo.AddAnimal(tiger);

            return tiger.ProducingSound();
        }
        public string CreateMouse(string name, double weight, string livingRegion)
        {
            IMammal mouse = new Mouse(name, weight, livingRegion);

            this._repo.AddAnimal(mouse);

            return mouse.ProducingSound();
        }
        public string CreateOwl(string name, double weight, double wingSize)
        {
            IBird owl = new Owl(name, weight, wingSize);

            this._repo.AddAnimal(owl);

            return owl.ProducingSound();
        }
        public string CreateHen(string name, double weight, double wingSize)
        {
            IBird hen = new Hen(name, weight, wingSize);

            this._repo.AddAnimal(hen);

            return hen.ProducingSound();
        }

        public IFood CreateFruit(int quantity)
        {
            return new Fruit(quantity);
        }
        public IFood CreateMeat(int quantity)
        {
            return new Meat(quantity);
        }
        public IFood CreateVegetable(int quantity)
        {
            return new Vegetable(quantity);
        }
        public IFood CreateSeeds(int quantity)
        {
            return new Seeds(quantity);
        }

        public string FeedAnimal(IFood food)
        {
            string message = String.Empty;
            try
            {
                IAnimal animalToFeed = this._repo.AnimalForFeeding();
                message = animalToFeed.Eat(food);
            }
            catch (InvalidOperationException ioe)
            {
                message = ioe.Message;
            }
            return message;
        }

        public string Print()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var animal in this._repo.Animals)
            {
                sb.AppendLine(animal.ToString());
            }

            return sb.ToString().TrimEnd();
        }
    }
}
