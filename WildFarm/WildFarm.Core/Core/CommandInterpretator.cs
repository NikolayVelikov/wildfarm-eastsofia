﻿using System;
using WildFarm.Core.Contracts;
using WildFarm.Models.Contracts.Food;

namespace WildFarm.Core.Core
{
    public class CommandInterpretator : ICommandInterpertator
    {
        private const string owl = "Owl";
        private const string hen = "Hen";
        private const string mouse = "Mouse";
        private const string dog = "Dog";
        private const string cat = "Cat";
        private const string tiger = "Tiger";
        private const string vegatable = "Vegetable";
        private const string fruit = "Fruit";
        private const string meat = "Meat";
        private const string seeds = "Seeds";

        private IService _service;
        public CommandInterpretator(IService service)
        {
            _service = service;
        }

        public string CreateAnimal(string[] input)
        {
            string animalType = input[0];
            string animalName = input[1];
            double animalWeight = double.Parse(input[2]);

            string message = String.Empty;
            switch (animalType)
            {
                case owl:
                    double owlWingSize = double.Parse(input[3]);
                    message = this._service.CreateOwl(animalName,animalWeight,owlWingSize);
                    break;
                case hen:
                    double henWingSize = double.Parse(input[3]);
                    message = this._service.CreateHen(animalName, animalWeight, henWingSize);
                    break;
                case mouse:
                    string mouseLivingRegion = input[3];
                    message = this._service.CreateMouse(animalName, animalWeight,mouseLivingRegion);
                    break;
                case dog:
                    string dogLivingRegion = input[3];
                    message = this._service.CreateDog(animalName, animalWeight, dogLivingRegion);
                    break;
                case cat:
                    string catLivingRegion = input[3];
                    string catBreed = input[4];
                    message =  this._service.CreateCat(animalName, animalWeight, catLivingRegion,catBreed);
                    break;
                case tiger:
                    string tigerLivingRegion = input[3];
                    string tigerBreed = input[4];
                    message = this._service.CreateTiger(animalName, animalWeight, tigerLivingRegion, tigerBreed);
                    break;
            }

            return message;
        }

        public string Feeding(string[] input)
        {
            IFood currentFood = null;
            string foodType = input[0];
            int foodQuantity = int.Parse(input[1]);

            switch (foodType)
            {
                case vegatable:
                    currentFood = this._service.CreateVegetable(foodQuantity);
                    break;
                case fruit:
                    currentFood = this._service.CreateFruit(foodQuantity);
                    break;
                case meat:
                    currentFood = this._service.CreateMeat(foodQuantity);
                    break;
                case seeds:
                    currentFood = this._service.CreateSeeds(foodQuantity);
                    break;
            }

            return this._service.FeedAnimal(currentFood);
        }

        public string Print()
        {
            return this._service.Print();
        }
    }
}
