﻿using System.Collections.Generic;
using WildFarm.Core.Contracts;
using WildFarm.Models.Contracts.Animal;

namespace WildFarm.Core.Core
{
    public class Repos : IRepository
    {
        private IList<IAnimal> _animals;

        public Repos()
        {
            this._animals = new List<IAnimal>();
        }

        public IReadOnlyCollection<IAnimal> Animals => (IReadOnlyCollection<IAnimal>)this._animals;

        public void AddAnimal(IAnimal animal)
        {
            this._animals.Add(animal);
        }

        public IAnimal AnimalForFeeding()
        {
            IAnimal animal = this._animals[this._animals.Count - 1];

            return animal;
        }
    }
}
