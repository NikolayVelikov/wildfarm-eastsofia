﻿using WildFarm.Core.Contracts;
using WildFarm.Core.IO.Contracts;

namespace WildFarm.Core.Core
{
    public class Engine : IEngine
    {
        private IReader _read;
        private IWriter _write;
        private ICommandInterpertator _interpreter;

        public Engine(IReader reader, IWriter writer, ICommandInterpertator interpeter)
        {
            this._read = reader;
            this._write = writer;
            this._interpreter = interpeter;
        }

        public void Run()
        {
            string input = string.Empty;
            while ((input = this._read.ReadLine()) != "End")
            {
                string message = string.Empty;

                try
                {
                    string[] token = input.Split(' ', System.StringSplitOptions.RemoveEmptyEntries);
                    string comand = token[0];
                    if (comand == "Vegetable" || comand == "Fruit" || comand == "Meat" || comand == "Seeds")
                    {
                        message = this._interpreter.Feeding(token);
                    }
                    else
                    {
                        message = this._interpreter.CreateAnimal(token);
                    }
                }
                catch (System.Exception ex)
                {
                    message = ex.Message;                    
                }


                this._write.WriteLine(message);
            }

            this._write.WriteLine(this._interpreter.Print());
        }
    }
}
