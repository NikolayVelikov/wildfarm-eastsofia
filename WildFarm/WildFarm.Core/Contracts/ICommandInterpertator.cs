﻿namespace WildFarm.Core.Contracts
{
    public interface ICommandInterpertator
    {
        string CreateAnimal(string[] input);
        string Feeding(string[] input);
        string Print();
    }
}