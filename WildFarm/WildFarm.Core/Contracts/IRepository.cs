﻿using System.Collections.Generic;
using WildFarm.Models.Contracts.Animal;

namespace WildFarm.Core.Contracts
{
    public interface IRepository
    {
        IReadOnlyCollection<IAnimal> Animals { get; }

        void AddAnimal(IAnimal animal);

        IAnimal AnimalForFeeding();
    }
}