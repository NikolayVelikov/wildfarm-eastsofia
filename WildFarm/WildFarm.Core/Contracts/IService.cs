﻿using WildFarm.Models.Contracts.Food;

namespace WildFarm.Core.Contracts
{
    public interface IService // implement Repository into the class
    {
        string CreateHen(string name, double weight, double wingSize);
        string CreateOwl(string name, double weight, double wingSize);
        string CreateMouse(string name, double weight, string livingRegion);
        string CreateDog(string name, double weight, string livingRegion);
        string CreateCat(string name, double weight, string livingRegion, string breed);
        string CreateTiger(string name, double weight, string livingRegion, string breed);
        IFood CreateVegetable(int quantity);
        IFood CreateFruit(int quantity);
        IFood CreateMeat(int quantity);
        IFood CreateSeeds(int quantity);
        string FeedAnimal(IFood food);
        string Print();
    }
}